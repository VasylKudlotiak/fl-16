const ten = 10;
const zero = 0;
function reverseNumber(num) {
 let revNum = zero;
 while(num > zero) {
     revNum = revNum * ten + num % ten;
     num = Math.floor(num / ten);
 } return revNum;
}

function forEach(arr, func) {
    for (let i = zero; i < arr.length; i++) {
        func(arr[i])
    } return arr;
}

function map(arr,func) {    
    return forEach(arr, func);
}

function filter(arr, func) {
  let newArray = [];
  
  forEach(arr, (el) => {
      if(func(el)) {
          newArray.push(el);
      }
  })
  return newArray;
}


function getAdultAppleLovers(data) {
 let addult = filter(data,(el) => {
   if (people.favoriteFruit === 'apple' && people.age >= 18) {
    return addult(el);
   } let mapArr = []
   forEach(arr , (el) => mapArr.push(func(el)) )
   return mapArr;
 });
} 

function getKeys(obj) {
    let keys = [];
    for (let key in obj) {
        keys.push(key)
    } return keys;
}

function getValues(obj) {
 let values = [];
 for(let value in obj) {
     values.push(obj[value]);
 } return values;
}

function showFormattedDate(dateObj) {
    const data = dateObj.toString();
    return `It is ${data.slice(8, 10)} of ${data.slice(4, 7)},${data.slice(10, 15)}`;
}

