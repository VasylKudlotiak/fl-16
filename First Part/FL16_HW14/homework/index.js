/* START TASK 1: Your code goes here */
let toggleHighlight = function (e) {
    let bg = e.target.style.backgroundColor;
    if (bg === 'yellow') {
        e.target.style.backgroundColor = '';
    } else {
        e.target.style.backgroundColor = 'yellow';   
    }
}

let prepareTable = function () {
    let cells = document.getElementsByTagName('td');
    for (let i = 0 ; i < cells.length ; i++) {
        cells[i].onclick = function(event) {
            toggleHighlight(event);
        }
    }
}
document.onload = prepareTable();

let toggleColor = function (e) {
    let bg = e.target.style.backgroundColor;
    if(bg === 'blue') {
        e.target.style.backgroundColor = '';
    } else {
        e.target.style.backgroundColor = 'blue'
    }
}

function changeRowColor() {
    let row = document.querySelectorAll('td:first-of-type');
    for(let i = 0; i < row.length; i++) {
        row[i].onclick = function(e) {
            toggleColor(e);
        }
    }
}
document.onload = changeRowColor();

function changeAll(e) {
    let bg = e.target.style.backgroundColor;
    if (bg === 'yellow') {
        e.target.style.backgroundColor = '';
    } else {
        e.target.style.backgroundColor = 'yellow';   
    }
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
let numInput = document.getElementById('phone');
let correct = document.getElementById('correct');
let wrong = document.getElementById('wrong');

function validateNum() {
    let regExp = /^\+380\d{9}$/gm;
    if (regExp.test(numInput.value)) {
        correct.classList.remove('hidden');
        wrong.classList.add('hidden');
    } else {
        wrong.classList.remove('hidden');
        correct.classList.add('hidden');
    } 
}    
/* END TASK 2 */

/* START TASK 3: Your code goes here */
let field = document.getElementById('field');
let ball = document.getElementById('ball');
field.onclick = function(event) {

    let fieldCoords = this.getBoundingClientRect();

    let ballCoords = {
      top: event.clientY - fieldCoords.top - field.clientTop - ball.clientHeight / 2,
      left: event.clientX - fieldCoords.left - field.clientLeft - ball.clientWidth / 2
    };

    if (ballCoords.top < 0) {
        ballCoords.top = 0;
    } 

    if (ballCoords.left < 0) {
        ballCoords.left = 0;
    }

    if (ballCoords.left + ball.clientWidth > field.clientWidth) {
      ballCoords.left = field.clientWidth - ball.clientWidth;
    }

    if (ballCoords.top + ball.clientHeight > field.clientHeight) {
      ballCoords.top = field.clientHeight - ball.clientHeight;
    }

    ball.style.left = ballCoords.left + 'px';
    ball.style.top = ballCoords.top + 'px';
}
/* END TASK 3 */
