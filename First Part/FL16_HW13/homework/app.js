const appRoot = document.getElementById('app-root');
appRoot.insertAdjacentHTML(
  'beforeend',
  `<div class = 'Countries_search_wrap'>
    <h1>Countries Search</h1>
    <form class = 'form'>
    <span class = 'searchType'>Please choose the type of search:</span>
    <label class = 'Region'>
    <input type ='radio' name ='TypeofSearch' id='regionInput'>By Region
    <br>
    <input type ='radio' name ='TypeofSearch' class='btn_lang' id='langInput'>By Language
    </label> 
    <br>
    <div class= 'searchquery'>
    <label class ='chooseSearch'> Please choose search query
    <select id='regionSelect'>Select value>
    <option>Select value</option>
    </select
    ></label>
    </div>
    </form>
    </div>
    <div class='countries_table_wrap'>
    <table class='sorting'>
    <tr>
    <th>Country Name</th>
    <th>Capital</th>
    <th>World Region</th>
    <th>Languages</th>
    <th>Area</th>
    <th>Flag</th>
    </tr>
    </table>
    </div>`
);

let select = document.getElementById('regionSelect');
const regionList = externalService.getRegionsList();
for(let i = 0; i < regionList.length; i++) {
    let opt = regionList[i];
    let el = document.createElement('option');
    el.textContent = opt;
    el.value = opt;
 const regionInput = document.getElementById('regionInput');
    if(regionInput.checked === true){
    select.appendChild(el);
    } 
}

const languageList = externalService.getLanguagesList();
for(let i = 0; i < languageList.length; i++) {
    let opt = languageList[i];
    let element = document.createElement('option');
    element.textContent = opt;
    element.value = opt;
    document.getElementById('langInput');
    if(langInput.checked === true){
    select.appendChild(element);
    } 
}

