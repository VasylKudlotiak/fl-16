function getAge() {  
    let today = new Date(2020, 9, 22);
    let birthDate = new Date(2000, 9, 23);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || m === 0 && today.getDate() < birthDate.getDate()) {
        age--;
 } return age;
}

function getWeekDay(dateString) {
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let d = new Date(dateString);
    let dayName = days[d.getDay()];
    return dayName;
} 

function getNumberOfDays(dateString) {
    let yearEnd = new Date(2020, 11, 31); 
    let today = new Date(dateString);
    let mlDay = 1000 * 3600 * 24 
    let daysNumber = yearEnd - today;
    return Math.ceil(daysNumber/mlDay);
} 

const getProgrammersDay = (year) => {
    if (getWeekDay(year) ) {
      return '12 Sep, ' + year + ' (' + getWeekDay(new Date(year, 8, 12)) + ')';
    } else {
      return '13 Sep, ' + year + ' (' + getWeekDay(new Date(year, 8, 13)) + ')';
    }
  } 
  
function untilDay(dateString) {
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const today = new Date();
    const currentDay = days[today.getDay()];
    if (currentDay === dateString) {
       return `Hey,today is ${currentDay} =)`;
    } else {
      return `It's ${0} day(s) left till ${dateString}`;
    }
} 

function validateInp(str) {
 let validInp = /^[A-Za-z_$][A-Za-z_$0-9]*$/
 if(validInp.test(str)) {
     return true;
 } else {
     return false;
 }
}

function toUpper(str) {
    return str.replace(/(^[a-z]| [a-z])/g, function (letter) {    
        return letter.toUpperCase();
      });
}

function validAudio(str) {
  let audioRegEx = /^[a-z|A-Z]+(.mp3|.flac|.alac|.aac)$/
  if(audioRegEx.test(str)) {
      return true;
  } else {
      return false;
  }
} 

const getColors = (string) => {
    const result = string.match(/#([a-f0-9]{3}){1,2}\b/gi);
    console.log(result);
}

function passwordCheck(str) {
    let passwordRegEx = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/
    if(passwordRegEx.test(str)) {
        return true;
    } else {
        return false;
    }
} 

function strToNum(str) {
    return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const getAllUrlsFromText = (url) => {
    return url.match(
/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_.~#?&//=]*)/g
);
}; 
