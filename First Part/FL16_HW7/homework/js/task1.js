  const initialAmount = parseInt(+prompt('Initial amount', ''));
  const numberOfYears = Number(+prompt('Number of years', ''));
  const percentage = parseInt(+prompt('Percentage of year','')); 
  const totalProfit = (initialAmount * Math.pow(1 + percentage/100, numberOfYears)- initialAmount).toFixed(2);
  const totalAmount = (initialAmount * Math.pow(1 + percentage/100, numberOfYears)).toFixed(2);
  const text = `Initial amount: ${initialAmount}
Number of years: ${numberOfYears}
Percentage of year: ${percentage}


Total profit: ${totalProfit}
Total amount: ${totalAmount}`;

  if(initialAmount < 1000 || numberOfYears < 1 || percentage > 100) {
      alert('Invalid input data');
  } else {
      window.alert(`${text}`);
  }
