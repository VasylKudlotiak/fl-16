function isEquals(a,b) {
    return a === b;
}

function isBigger(a,b) {
    return a > b;
}

function storeNames() {
    let storeNames = [];
    for(let i = 0; i < arguments.length; i++) {
        storeNames.push(arguments[i]);
    }
    return storeNames;
}

function getDifference (a, b) {
    if(a > b){
        let difference = a - b;
        return difference;
    } else {
        let difference = b - a;
        return difference;
    } 
} 

const arr = [1, -3, -5, -8];
function countNegative(arr) {
    let counter = [0];
    arr.forEach(function(a) {
        if(a < 0) {
            counter [0]++;
        } 
    }); 
    return counter;
}

function letterCount(str, letter) {
 let letter_Count = 0;
 for (let position = 0; position < str.length; position++) {
    if (str.charAt(position) === letter) {
     letter_Count += 1;
      }
  }
  return letter_Count;
} 


function countPoints(arr) {
    let counter = 0;
    for (let i = 0; i < arr.length; i++) {
      let three = 3;
      let num = arr[i].split(':');
      let x = +num[0]; let y = +num[1]
      if (x > y) {
        counter += three;
      } else if (x < y) {
        counter += 0;
      } else if (x === y) {
        counter += 1;
      }
    }
    return counter;
}
