let pageCount1 = 1;
let pageCount2 = 1;
let pageCount3 = 1;
function visitLink(path) {
    switch(path) {
        case 'Page1':
            if(!localStorage.getItem('pageCount1')) {
                localStorage.setItem('pageCount1', pageCount1);
            } else {
                pageCount1 = localStorage.getItem('pageCount1');
                pageCount1 = parseInt(pageCount1) + 1;
                localStorage.setItem('pageCount1', pageCount1);
            } break;
 
            case 'Page2':
            if(!localStorage.getItem('pageCount2')) {
                localStorage.setItem('pageCount2', pageCount2);
            } else {
                pageCount2 = localStorage.getItem('pageCount2');
                pageCount2 = parseInt(pageCount2) + 1;
                localStorage.setItem('pageCount2', pageCount2);
            } break;
 
            case 'Page3':
            if(!localStorage.getItem('pageCount3')) {
                localStorage.setItem('pageCount3', pageCount3);
            } else {
                pageCount3 = localStorage.getItem('pageCount3');
                pageCount3 = parseInt(pageCount3) + 1;
                localStorage.setItem('pageCount3', pageCount3);
            } break;
    }    
 }

function viewResults() {
    let element = document.getElementById('content');
    let list = document.createElement('ul');
    let li1 = document.createElement('li');
    let li2 = document.createElement('li');
    let li3 = document.createElement('li');

    li1.innerHTML = `You visited Page1 ${localStorage.getItem('pageCount1')} time(s)`;
    li2.innerHTML = `You visited Page2 ${localStorage.getItem('pageCount2')} time(s)`;
    li3.innerHTML = `You visited Page3 ${localStorage.getItem('pageCount3')} time(s)`;

    element.appendChild(list);
    element.appendChild(li1);
    element.appendChild(li2);
    element.appendChild(li3);
    localStorage.clear();
}