class Magazine {
    constructor(ReadyForPushNotification, ReadyForApprove, ReadyForPublish, PublishInProgress) {
        this.ReadyForPushNotification = ReadyForPushNotification;
        this.ReadyForApprove = ReadyForApprove;
        this.ReadyForPublish = ReadyForPublish;
        this.PublishInProgress = PublishInProgress;
    }
}
class ReadyForPushNotification {
    approve(name) {
        console.log(`Hello ${name}. You can't approve. We don't have enough of publications.`);
    }

    publish(name) {
        console.log(`Hello ${name}. You can't publish. We are creating publications now.`);
    }
}

class ReadyForApprove {
    approve(name) {
        console.log(`Hello ${name} You can't publish. We don't have a manager's approval.`);
    }

    publish(name) {
        console.log(`Hello ${name} You've approved the changes`);
    }
}

class ReadyForPublish {
    approve(name) {
        console.log(`Hello ${name} You've recently published publications.`);
    }

    publish(name) {
        console.log(`Hello ${name} Publications have been already approved by you.`);
    }
}

class PublishInProgress {
    approve(name) {
        console.log(`Hello ${name}. While we are publishing we can't do any actions`);
    }

    publish(name) {
        console.log(`Hello ${name}. While we are publishing we can't do any actions.`);
    }
}