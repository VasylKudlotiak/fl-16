//1
const arr = ['1', '2', '3', '4', '5'].map(Number);
function getBiggestEven (arr) {
    let evens = arr.filter(number => number % 2 === 0);
    return Math.max(... evens);
}

let a = 3;
let b = 5;

[a, b] = [b, a];

const getValue = (value) => value ?? '-'; 

const arrayOfArrays = [
    ['name', 'Dan'],
    ['age', '21'],
    ['city', 'Lviv']
];
const obj = Object.fromEntries(arrayOfArrays);

const obj1 = {name: 'nick'};
const addUniqueId = obj1 => {
    let id = Symbol();
    return {...obj1, id};
};

const oldObj = {
    name: 'willow',
    details: { id: 1, age: 47, univercity: 'LNU'}
};
const getRegroupedObject = obj => {
    const {
        name: firstName,
        details: { id, age, univercity }
    } = obj;
    const user = { age, firstName, id};
    return {univercity, user};
};

const arr1 = [2, 3, 4, 2, 4, 'a', 'c', 'a'];
const getArrayWithUniqueElements = (arr1) => [...new Set(arr1)];

const phoneNumber = '0123456789';
function hiddenNumber (phoneNumber) {
   let lastfour = phoneNumber.slice(-4).padStart(phoneNumber.length, '*');
    return lastfour.padStart(5, '*');
}

function add (a, b) {
    if(a === undefined || b === undefined) {
        throw 'Error: b is required';
    } else {
       return a + b;
    }
}

const generatorObject = generateIterableSequence();
function * generateIterableSequence () {
    yield 'I';
    yield 'Love';
    yield 'Epam'
}
for (let value of generatorObject) {
    console.log(value);
}