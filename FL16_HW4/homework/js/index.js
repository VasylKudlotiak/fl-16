'use strict';
class Pizza {
    constructor (size, type) {
        if( size === undefined || type === undefined) {
            throw new PizzaException(`Required two arguments. given ${arguments.length}`);
        } else if (Pizza.allowedSizes.indexOf(size) === -1 || Pizza.allowedTypes.indexOf(type) === -1) {
            throw new PizzaException(`Invalid type`);
        } 
        this.size = size[0]
        this.type = type[0]
        this.extra = []
        this.extraPrice = []
        this.sizePrice = size[1]
        this.typePrice = type[1]
    }
    addExtraIngredient(ingredient) {
        return this.extra.push(ingredient[0]) && this.extraPrice.push(ingredient[1])
    }
    removeExtraIngredient(ingredient) {
            let reming = this.extra.indexOf(ingredient[0])
            return this.extra.splice(reming, 1) && this.extraPrice.splice(reming, 1)             
}
    getSize(){
        return this.size
    }
    getPrice() {
        return +this.extraPrice.reduce((a, b) => a + b) + +this.sizePrice + +this.typePrice
    }
    getExtraIngredients() {
        return this.extra;
    }
    getPizzaInfo() {
        return `Size: ${this.size}, type: ${this.type}; extra ingredients: ${this.extra}; price: ${this.getPrice()}UAH.`
    }
}

    Pizza.SIZE_S = ['SMALL', 50]
    Pizza.SIZE_M = ['MEDIUM', 75]
    Pizza.SIZE_L = ['LARGE', 100]

    Pizza.TYPE_VEGGIE = ['VEGGIE', 50]
    Pizza.TYPE_MARGHERITA = ['MARGHERITA', 60]
    Pizza.TYPE_PEPPERONI = ['PEPPERONI', 70]

    Pizza.EXTRA_TOMATOES = ['TOMATOES', 5]
    Pizza.EXTRA_CHEESE = ['CHEESE', 7]
    Pizza.EXTRA_MEAT = ['MEAT', 9]

   Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
   Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
   Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];

class PizzaException {
    constructor(message) {
        this.log = message;
    }
}
