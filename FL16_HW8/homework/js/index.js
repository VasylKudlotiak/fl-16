$(document).ready(function(){
    let numOne;
    let numTwo;
    let operator;
    let $result = $('.total');

function reset(){
    numOne=null;
    numTwo=null;
    operator=null;
    $result.val('0');
}

reset();

$('.numbers button').click(function(){
    let clickDigit = $(this).text();
    let currentVal = $result.val();
    let newVal;

    if (currentVal === '0'){
            newVal=clickDigit;
        } else {
    newVal = currentVal + clickDigit;
}
$result.val(newVal);
   

});

$('.operators button').click(function(){
    operator = $(this).text();
    numOne=parseFloat($result.val());
    $result.val('0');
});

$('#equal').click(function(){
    let total;

    numTwo = parseFloat($result.val());

    if (operator=== '+'){
            total = numOne + numTwo;
    } else if (operator === '-'){
            total = numOne - numTwo;
    } else if (operator === '*'){
            total = numOne * numTwo;
    } else if (operator === '/'){
            total = numOne / numTwo;
    }
    $result.val(total);
});

$('#delete').click(function(){
    reset();
});

});