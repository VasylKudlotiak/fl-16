fetch('https://jsonplaceholder.typicode.com/users').then(
  res => {
    res.json().then(
      data => {
        console.log(data);
        if (data.length > 0) {

          let temp = '';
          data.forEach((itemData) => {
            temp += '<tr>';
            temp += '<td>' + itemData.id + '</td>';
            temp += '<td>' + itemData.name + '</td>';
            temp += '<td>' + itemData.username + '</td>';
            temp += '<td>' + itemData.email + '</td>';
            temp += '<td>' + itemData.address.city +'</td>';
            temp += '<td>' + itemData.phone + '</td>';
            temp += '<td>' + itemData.website + '</td>';
            temp += '<td>' + itemData.company.name + '</td>';
            temp += '<td><button>Edit</button></td>';
            temp += '<td><button onclick = delRow(this)>Delete</button></td></tr>';
          });
          document.getElementById('data').innerHTML = temp;
        }
      }
    )
  }
)
function delRow(x){
    document.getElementById('data').deleteRow(x.parentNode.parentNode.rowIndex);
}
